#*************************************
# Honor Code: This work is mine unless otherwise cited.
# Janyl Jumadinova
# CMPSC 300 Spring 2016
# Class Example
# Date: February 11, 2016

# Purpose: Reading file with BioPython
#*************************************

from Bio import SeqIO

my_file = open('Diabetes.fasta')

for record in SeqIO.parse(my_file,'fasta'):
	id = record.id
	seq = record.seq
	print 'Name: ', id 
	print 'Size: ', len(seq)
	print 'Sequence: ', seq
	
my_file.close()

